<?php

namespace Tests\Feature;

use App\Models\AutomobilePart;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AutoMobileControllerTest extends TestCase
{
    use DatabaseTransactions,DatabaseMigrations;

    public function testIndex ()
    {
        $testNumber = 4;
        $testQuantity = 3;
        factory(AutomobilePart::class)->create([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ]);
        $response = $this->get(route('automobile.parts.index'));
        $response->assertStatus(200)->assertJsonCount(1);
    }

    public function testStoreWith ()
    {
        $testNumber = 4;
        $testQuantity = 3;
        $partsCount = 4;
        $testValues = factory(AutomobilePart::class, $partsCount)->make([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ])->toArray();
        $data['parts'] = [];
        foreach ($testValues as $key => $value) {
            $data['parts'][$key] = $value;
        }
        $this->post(route('automobile.parts.store'),$data);
        $this->assertDatabaseHas('automobile_parts', [
            'number' => $testNumber,
            'quantity' => $testQuantity * $partsCount
        ]);
    }
}
