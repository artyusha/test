<?php

namespace Tests\Unit\Requests;

use App\Contracts\AutoMobilePartContract;
use App\Http\Requests\AutoMobilePartRequest;
use App\Models\AutomobilePart;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AutoMobilePartRequestTest extends TestCase
{
    /**
     * @var AutoMobilePartRequest
     */
    protected $_object;

    public function setUp()
    {
        parent::setUp();
        $mockedRepo = \Mockery::mock(AutoMobilePartContract::class);
        $mockedRepo->shouldReceive('exists')->andReturn(true);
        $this->_object = new AutoMobilePartRequest([],[],[],[],[],[],null, $mockedRepo);
    }

    public function testExpectJsonIsTrue ()
    {
        $this->assertTrue( $this->_object->expectsJson());
    }

    public function testAuthorizeIsTrue ()
    {
        $this->assertTrue( $this->_object->authorize());
    }

    public function testInputsExistsIsTrue ()
    {
        $testNumber = 4;
        $testQuantity = 3;
        $partsCount = 4;
        $testValues = factory(AutomobilePart::class, $partsCount)->make([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ])->toArray();
        $data['parts'] = [];
        foreach ($testValues as $key => $value) {
            $data['parts'][$key] = $value;
        }

        $this->_object->replace($data);
        $this->assertTrue(count($this->_object->inputs()['create']) == 0 && count($this->_object->inputs()['update']) == 1);
    }

    public function testRulesIsArray ()
    {
        $this->assertTrue(is_array($this->_object->rules()));
    }

    public function testInputsExistsIsFalse ()
    {
        $mockedRepo = \Mockery::mock(AutoMobilePartContract::class);
        $mockedRepo->shouldReceive('exists')->andReturn(false);
        $this->_object = new AutoMobilePartRequest([],[],[],[],[],[],null, $mockedRepo);
        $testNumber = 4;
        $testQuantity = 3;
        $partsCount = 4;
        $testValues = factory(AutomobilePart::class, $partsCount)->make([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ])->toArray();
        $data['parts'] = [];
        foreach ($testValues as $key => $value) {
            $data['parts'][$key] = $value;
        }

        $this->_object->replace($data);
        $this->assertTrue(count($this->_object->inputs()['create']) == 1 && count($this->_object->inputs()['update']) == 0);
    }



}
