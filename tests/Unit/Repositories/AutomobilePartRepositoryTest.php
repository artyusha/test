<?php

namespace Tests\Unit\Repositories;

use App\Contracts\AutoMobilePartContract;
use App\Models\AutomobilePart;
use App\Repositories\AutoMobilePartRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AutomobilePartRepositoryTest extends TestCase
{
    use DatabaseTransactions,DatabaseMigrations;

    const TESTED_VALUES_COUNT_MIN = 4;

    const TESTED_VALUES_COUNT_MAX = 12;
    /**
     * @var AutoMobilePartRepository
     */
    protected $_object;

    public function setUp()
    {
        parent::setUp();
        $this->_object = $this->app->make(AutoMobilePartContract::class);
    }

    public function testModel ()
    {
        $this->assertTrue($this->_object->model() instanceof AutomobilePart);
    }

    public function testAllWithCacheWithCache ()
    {
        $testValues = $this->_generateAutomobilePartsTestValues();
        Cache::shouldReceive('rememberForever')
            ->with($this->any())
            ->andReturn($testValues);

        $this->assertEquals($testValues->toArray(), $this->_object->allWithCache()->toArray());
    }

    public function testAllWithCacheNoCache ()
    {
        $testValues = $this->_generateAutomobilePartsTestValues();
        $this->assertEquals($testValues->toArray(), $this->_object->allWithCache()->toArray());
    }

    public function testUpdateQuantity ()
    {
        $testNumber = 4;
        $testQuantity = 3;
        $partsCount = 4;
        factory(AutomobilePart::class)->create([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ]);
        $testValues = factory(AutomobilePart::class, $partsCount)->make([
            'number' => $testNumber,
            'quantity' => $testQuantity
        ])->toArray();
        $this->_object->updateQuantity($testValues);
        $this->assertDatabaseHas('automobile_parts', [
            'number' => $testNumber,
            'quantity' => $testQuantity * ($partsCount + 1)
        ]);
    }

    /**
     * @return Collection
     */
    private function _generateAutomobilePartsTestValues ()
    {
        return factory(AutomobilePart::class, mt_rand(self::TESTED_VALUES_COUNT_MIN, self::TESTED_VALUES_COUNT_MIN))
            ->create();
    }
}
