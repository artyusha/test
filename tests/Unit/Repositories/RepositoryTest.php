<?php

namespace Tests\Unit\Repositories;

use App\Models\AutomobilePart;
use App\Repositories\Repository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RepositoryTest extends TestCase
{
    use DatabaseMigrations,DatabaseTransactions;

    protected $_object;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        parent::setUp();
        $this->_object = $this->getMockForAbstractClass(Repository::class);
        $this->_object->expects($this->any())
            ->method('model')
            ->will($this->returnValue(new AutomobilePart()));

    }

    public function testExistsIsTrue ()
    {
        $testNumber = 45;
        factory(AutomobilePart::class)->create([
            'number' => $testNumber
        ]);
        $this->assertTrue($this->_object->exists([
            'number' => $testNumber
        ]));
    }

    public function testExistsIsFalse ()
    {
        $testNumber = 45;
        factory(AutomobilePart::class)->create([
            'number' => $testNumber
        ]);
        $this->assertFalse($this->_object->exists([
            'number' => $testNumber + 1
        ]));
    }

    public function testCreate ()
    {
        $testPart = factory(AutomobilePart::class)->make()->toArray();
        $this->_object->create($testPart);
        $this->assertDatabaseHas('automobile_parts', $testPart);
    }

    public function testUpdate ()
    {
        $existPart = factory(AutomobilePart::class)->create();
        $testPart = factory(AutomobilePart::class)->make()->toArray();
        $this->_object->update($testPart, $existPart->id);
        $testPart['id'] = $existPart->id;
        $this->assertDatabaseHas('automobile_parts', $testPart);
    }

    public function testInsert ()
    {
        $testPart = factory(AutomobilePart::class)->make()->toArray();
        $this->_object->insert($testPart);
        $this->assertDatabaseHas('automobile_parts', $testPart);
    }

}
