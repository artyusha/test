<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\AutomobilePart::class, function (Faker $faker) {
    return [
        'number' => $faker->unique()->buildingNumber,
        'description' => $faker->text,
        'brand_name' => $faker->company,
        'quantity'  => $faker->numberBetween(1),
        'price'     => $faker->numberBetween(1,50000),
        'condition' => $faker->randomElement(['new','paid']),
        'location'  => $faker->company,
    ];
});
