<?php

namespace App\Http\Controllers;

use App\Contracts\AutoMobilePartContract;
use App\Http\Requests\AutoMobilePartRequest;
use Illuminate\Cache\CacheManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AutomobilePartController extends Controller
{
    protected $repo;

    /**
     * AutomobilePartController constructor.
     * @param AutoMobilePartContract $mobilePartContract
     */
    public function __construct(AutoMobilePartContract $mobilePartContract)
    {
        $this->repo = $mobilePartContract;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index ()
    {
        $result = $this->repo->allWithCache();
        return $this->_generateJsonResponse($result);
    }

    /**
     * @param AutoMobilePartRequest $request
     * @param CacheManager $cache
     * @return \Illuminate\Http\JsonResponse
     */
    public function store (AutoMobilePartRequest $request, CacheManager $cache)
    {
        $creatingData = $request->inputs()['create'];
        $updatingData = $request->inputs()['update'];
        DB::transaction( function () use ($creatingData, $updatingData){
            $this->repo->insert($creatingData);
            $this->repo->updateQuantity($updatingData);
        });
        //Reset cache
        $cache->forget('automobile:parts');

        return $this->_generateJsonResponse(null, 201);
    }
}
