<?php

namespace App\Http\Requests;

use App\Contracts\AutoMobilePartContract;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class AutoMobilePartRequest extends FormRequest
{
    protected $repo;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null, AutoMobilePartContract $automobileRepo)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->repo = $automobileRepo;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parts' => 'required|array',
            'parts.*.number' => 'required|numeric',
            'parts.*.description' => 'required',
            'parts.*.brand_name' => 'required|string',
            'parts.*.quantity' => 'required|numeric',
            'parts.*.price'   => 'required|numeric',
            'parts.*.condition' => 'required|string',
            'parts.*.location'  => 'required|string'
        ];
    }

    /**
     * @return array
     */
    public function inputs ()
    {
        $inputs = $this->except('_token');
        return $this->filterDuplicatedValuesAndAddQuantity($inputs['parts']);
    }

    /**
     * @param array $inputs
     * @return array
     */
    private function filterDuplicatedValuesAndAddQuantity (array  $inputs)
    {
        $numbers = [];
        $creatingData = [];
        $updatingData = [];
        foreach ($inputs as $input) {
            if (in_array($input['number'], $numbers)) {
                if ($this->repo->exists(['number' => $input['number']])) {
                    $updatingData [$input['number']]['quantity'] += $input['quantity'];
                } else {
                    $creatingData [$input['number']]['quantity'] += $input['quantity'];
                }
            } else {
                if ($this->repo->exists(['number' => $input['number']])) {
                    $updatingData [$input['number']] = $input;
                } else {
                    $creatingData [$input['number']] = $input;
                }
            }
            $numbers[] = $input['number'];
        }
        return ['create' => $creatingData, 'update' => $updatingData];
    }

    public function expectsJson()
    {
        return true;
    }
}
