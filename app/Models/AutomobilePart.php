<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutomobilePart extends Model
{
    public $fillable = [
        'number',
        'description',
        'brand_name',
        'quantity',
        'price',
        'condition',
        'location',
    ];
}
