<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 10/17/18
 * Time: 9:45 PM
 */

namespace App\Contracts;


interface AutoMobilePartContract extends Repository
{
    public function updateQuantity (array $data);

    public function allWithCache ();
}