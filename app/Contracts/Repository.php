<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 10/18/18
 * Time: 12:57 AM
 */

namespace App\Contracts;


interface Repository
{
    /**
     * @return mixed
     */
    public function all ();
    /**
     * @param array $attr
     * @return mixed
     */
    public function create (array $attr);

    /**
     * @param array $attr
     * @return mixed
     */
    public function insert (array $attr);

    /**
     * @param array $attr
     * @param int $id
     * @return mixed
     */
    public function update (array $attr, int $id);


    /**
     * @param array $attr
     * @return mixed
     */
    public function exists (array $attr);
}