<?php

namespace App\Providers;

use App\Contracts\AutoMobilePartContract;
use App\Repositories\AutoMobilePartRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AutoMobilePartContract::class,
            AutoMobilePartRepository::class
        );
    }
}
