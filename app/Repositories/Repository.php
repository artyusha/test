<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements \App\Contracts\Repository {

    protected $model;

    public function __construct()
    {
        $this->model = $this->model();
    }

    protected  abstract function model ();

    /**
     * @return mixed
     */
    public function all ()
    {
        return ($this->model())->all();
    }
    /**
     * @param array $attr
     * @return mixed
     */
    public function create (array $attr)
    {
        return ($this->model())->create($attr);
    }

    /**
     * @param array $attr
     * @return mixed
     */
    public function insert (array $attr)
    {
        return ($this->model())->insert($attr);
    }

    /**
     * @param array $attr
     * @param int $id
     * @return mixed
     */
    public function update (array $attr, int $id)
    {
        return ($this->model())->where('id', $id)->update($attr);
    }



    /**
     * @param array $attr
     * @return mixed
     */
    public function exists(array $attr)
    {
        return ($this->model())->where($attr)->exists();
    }

}