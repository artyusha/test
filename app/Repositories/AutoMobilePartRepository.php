<?php
/**
 * Created by PhpStorm.
 * User: artur
 * Date: 10/17/18
 * Time: 9:46 PM
 */

namespace App\Repositories;


use App\Contracts\AutoMobilePartContract;
use App\Models\AutomobilePart;
use Illuminate\Cache\CacheManager;

class AutoMobilePartRepository extends Repository implements AutoMobilePartContract
{
    protected $cache;

    public function __construct (CacheManager $cache)
    {
        parent::__construct();
        $this->cache = $cache;
    }

    /**
     * @return string
     */
    public function model()
    {
       return new AutomobilePart();
    }

    /**
     * @param array $data
     */
    public function updateQuantity (array $data)
    {
        foreach ($data as $part) {
            $this->updateQuantityByNumber($part['quantity'], $part['number']);
        }
    }

    /**
     * @param $quantity
     * @param $number
     * @return mixed
     */
    protected function updateQuantityByNumber ($quantity, $number)
    {
        $oldQuantity = $this->model->where('number', $number)->first()->quantity;
        return $this->model->where('number', $number)->update([
            'quantity' => $oldQuantity + $quantity
        ]);
    }

    public function allWithCache()
    {
        return $this->cache->rememberForever('automobile:parts', function () {
           return $this->all();
        });
    }
}